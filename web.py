from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.common.exceptions import ElementClickInterceptedException, NoSuchElementException, WebDriverException


class Web:
    def __init__(self, command_line_args):
        self.command_line_args = command_line_args
        self.username = command_line_args.username
        self.password = command_line_args.password

        self.driver = self._create_web_driver(command_line_args)

    @staticmethod
    def _create_web_driver(user_args) -> Firefox:
        print(f'Creating web driver...')
        try:
            if not user_args.debug:
                options = FirefoxOptions()
                options.add_argument('--headless')
                return Firefox(options=options)
            else:
                return Firefox()
        except WebDriverException as e:
            print(e)
            exit()
        except KeyboardInterrupt:
            exit()

    def login(self):
        LOGIN_URL = 'https://www.noip.com/login'
        self.driver.get(LOGIN_URL)
        self.insert_username()
        self.insert_password()
        self.click_login()

    def insert_password(self):
        form_input = self.driver.find_element_by_name('password')
        form_input.send_keys(self.password)

    def insert_username(self):
        form_input = self.driver.find_element_by_name('username')
        form_input.send_keys(self.username)

    def click_login(self):
        login_button = self.driver.find_element_by_name('Login')
        login_button.click()

    def quit(self):
        self.driver.quit()
