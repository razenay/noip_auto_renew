#!/usr/bin/python3
from selenium.common.exceptions import ElementClickInterceptedException, NoSuchElementException, WebDriverException
from web import Web
import time
import argparse
import platform


def nav(driver):
    # Go to active hostname page
    try:
        active_host_btn = driver.find_element_by_xpath(
            '/html/body/div[1]/div/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div/div/div/div/div')
        active_host_btn.click()
    except NoSuchElementException:
        print('\nERROR\n\n1.) Your account is probably locked out for 15 Mins: try -d option for GUI to show error\n2.) Did you enter your username/password right?\n')
        return
    # Needed or won't have enough time to fetch classes
    time.sleep(3)
    # count active hostnames
    total_hostnames = driver.find_elements_by_class_name('table-striped-row')

    for items in total_hostnames:
        try:
            items.find_element_by_class_name('text-right-md').click()
            # TODO: have this print not show up if Renewal is not needed
            print('Your domain names have been updated.')
        except ElementClickInterceptedException:
            print('\nRenewal of domain(s) not needed\n')
            expire = driver.find_elements_by_class_name('no-link-style')
            domains = driver.find_elements_by_class_name('cursor-pointer')
            domains_dic = {}
            expired_times = {}
            i = 0
            j = 0
            k = 0
            for domain in domains:
                domains_dic[i] = domain.text
                i += 1
            for expire_time in expire:
                expired_times[j] = expire_time.text
                j += 1
            for domain_name in domains_dic:
                print(domains_dic[domain_name], ":", expired_times[k])
                k += 1
            print('\n')
            return


def get_command_line_arguments():
    parser = argparse.ArgumentParser(
        description='Used to auto renew noip certs')
    parser.add_argument('-u', '--username', action='store',
                        required=True, help='username or email')
    parser.add_argument('-p', '--password', action='store',
                        required=True, help='password')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='run Firefox without being headless')
    return parser.parse_args()


def main():

    command_line_arguments = get_command_line_arguments()
    if os_is_supported():
        web_driver = Web(command_line_arguments)
        web_driver.login()
        nav(web_driver.driver)
        web_driver.quit()


def os_is_supported() -> bool:
    if platform.machine() not in ("AMD64", "x86_64", "arm7l", "aarch64"):
        print('\nPlatform currently not supported yet!\n')
        return False
    return True


if __name__ == '__main__':
    main()
